/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'sarabun': ['Sarabun', 'sans-serif'],
      },
      width: {
        '500': '500px',
        '700': '700px'
      },
    },

  },
  plugins: [],
}