# Persional money management Project
#### Author/Developer: Tran Manh Dung - Cloud Engineer - Viettel Solutions
## Feature

- Manage balance/income/outcome amount.
- View transaction history.
- Add new transaction.
- Delete unnecessary transaction.

## Tech stack

- Vuejs
- Tailwind CSS

## UI Review

![img.png](readme-images/img3.png)

![img.png](readme-images/img2.png)

![img.png](readme-images/img1.png)

![img.png](readme-images/img4.png)